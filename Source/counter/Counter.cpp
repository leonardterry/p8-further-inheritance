//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Leonard Terry on 07/11/2016.
//
//

#include "Counter.h"

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Counter::Counter() : Thread("CounterThread")
{
    listener = nullptr;
    timeInterval = 1000;
    counterValue = 0;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Counter::~Counter()
{
    stopThread(1000); //stops the run function when called
    DBG("Destructed");
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void Counter::setListener (Listener* newListener)
{
    listener = newListener;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void Counter::run()
{
    
    DBG("Entered Run Function");
    
    uint32 counterValue = 0; //resets counter value each time thread is called
    
    while (!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
        
        if (listener != nullptr)
            listener->counterChanged (counterValue++);
        
//        std::cout << "Counter value: " << counterValue << "\n";
        
        Time::waitForMillisecondCounter(time + (60000.0 / timeInterval)); //pause loop for time interval length (tempo)
    }
    
    DBG("Exit Run Function");
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
bool Counter::isRunning()
{
    std::cout << "Thread state is: " << isThreadRunning() << std::endl;
    return isThreadRunning(); //returns state 'true' if thread is running when called
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void Counter::startCounterThread()
{
    startThread();  //starts the run function when called
    DBG("Counter Thread Started");
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void Counter::stopCounterThread()
{
    stopThread(1000); //stops the run function when called
    DBG("Counter Thread Stopped");
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void Counter::setTempo (int newTempo)
{
    timeInterval = newTempo;
    std::cout << "Time interval is: " << timeInterval << " BPM" << std::endl;
}
