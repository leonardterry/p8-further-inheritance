/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "../counter/Counter.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel,
                        public Slider::Listener,
                        public Button::Listener,
                        public Counter::Listener,
                        public ComboBox::Listener

{
public: 
    //==============================================================================
    /** Constructor */
    MainComponent (Audio& a);

    /** Destructor */
    ~MainComponent();

    void resized() override;
    
    void initialiseGainSlider();
    
    void sliderValueChanged (Slider* slider) override;
    
    void buttonClicked (Button* button) override;
    
    void counterChanged (const unsigned int counterValue) override;

    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    
    
    
private:
    Audio& audio;
    
    /** sliders */
    Slider gainSlider;
    Slider tempoSlider;
    
    /** buttons */
    TextButton counterStartStopButton;
   
    /** combo box's */
    ComboBox oscillatorComboBox;

    /** counters */
    Counter counter;
    
    /** string lists */
    //    String oscillatorItems[3];

    
    // ==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
