/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//=============================================================================='
MainComponent::MainComponent (Audio& a) : audio (a) //CONSTRUCTOR
{
    
    setSize (500, 400);
    
    /** Counter Start Stop Button */
    counterStartStopButton.setButtonText("Counter Button Start/Stop");
    counterStartStopButton.setClickingTogglesState(true);
    addAndMakeVisible(counterStartStopButton);
    counterStartStopButton.addListener(this);
    
    /** Tempo Slider */
    tempoSlider.setSliderStyle(juce::Slider::IncDecButtons);
    tempoSlider.setRange(60.0, 220.0, 1.0);
    tempoSlider.setValue(100);
    addAndMakeVisible(tempoSlider);
    tempoSlider.addListener(this);
    
    /** Oscillator Combo Box */
//    String oscillatorItems[4] = {"Sine", "Square", "Sawtooth", "Triangle"};
//    oscillatorComboBox.addItemList(oscillatorItems[3], 1);
    
    oscillatorComboBox.addItem("Sine", 1);
    oscillatorComboBox.addItem("Square", 2);
    oscillatorComboBox.addItem("Sawtooth", 3);
    oscillatorComboBox.addItem("Triangle", 4);
    addAndMakeVisible(oscillatorComboBox);
    oscillatorComboBox.addListener(this);
    
    /** Counter */
    counter.setListener(this);
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
MainComponent::~MainComponent() //DESTRUCTOR
{
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void MainComponent::resized() //CHANGE SIZE
{
//    gainSlider.setBounds(10, 10, 100, getHeight()-20);
    counterStartStopButton.setBounds(10, 10, getWidth()-20, 100);
    tempoSlider.setBounds(10, 120, (getWidth()-20)/2, 25);
    oscillatorComboBox.setBounds(10+((getWidth()-20)/2), 120, (getWidth()-20)/2, 25);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void MainComponent::sliderValueChanged (Slider* slider)
{
    if (slider == &gainSlider){
        std::cout << "gainSlider: " << gainSlider.getValue() << std::endl;
        //audio.setGain(gainSlider.getValue());
    }
    
    if (slider == &tempoSlider){
        std::cout << "tempoSlider: " << tempoSlider.getValue() << std::endl;
        counter.setTempo(tempoSlider.getValue());
    }
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void MainComponent::buttonClicked (Button* button)
{
    // Counter Button Start/Stop
    if (button == &counterStartStopButton)
    {
      
        DBG("counter button pressed");
        std::cout << "counter button state: " << counterStartStopButton.getToggleState() << std::endl;
        
        //Conditional statement to check thread is not already running
        if (counter.isRunning() == true)
            counter.stopCounterThread();
        else
            counter.startCounterThread();
        
//        if (counterStartStopButton.getToggleState() == false)
//            counter.stopCounterThread();
    }
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void MainComponent::counterChanged(const unsigned int counterValue)
{
    std::cout << "Counter Value: " << counterValue << std::endl;
    
    audio.beep();
   
}

void MainComponent::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    DBG("comboBoxChanged");
    
    if (comboBoxThatHasChanged == &oscillatorComboBox)
    {
        if (oscillatorComboBox.getSelectedItemIndex() == 0)
        {
            DBG("Sine Selected");
            audio.setOscillatorWaveform(oscillatorComboBox.getSelectedItemIndex());
        }
        else if (oscillatorComboBox.getSelectedItemIndex() == 1)
        {
            DBG("Square Selected");
            audio.setOscillatorWaveform(oscillatorComboBox.getSelectedItemIndex());
        }
        else if (oscillatorComboBox.getSelectedItemIndex() == 2)
        {
            DBG("Sawtooth Selected");
            audio.setOscillatorWaveform(oscillatorComboBox.getSelectedItemIndex());
        }
        else if (oscillatorComboBox.getSelectedItemIndex() == 3)
        {
            DBG("Triangle Selected");
            audio.setOscillatorWaveform(oscillatorComboBox.getSelectedItemIndex());
        }
        else
            DBG("ERROR - comboBoxChanged");
    }
}


//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

