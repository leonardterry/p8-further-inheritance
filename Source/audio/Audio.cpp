/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/**  */
Audio::Audio()
{
    oscillatorPtr.set(new SinOscillator());
    
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/**  */
Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
    delete oscillatorPtr.get();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/**  */
void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    std::cout << "note velo: " << message.getVelocity() << std::endl;
//    sineOscillator.setFrequency(message.getMidiNoteInHertz(message.getNoteNumber()));
//    sineOscillator.setAmplitude(message.getVelocity()/127.f);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/**  */
void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    
    while(numSamples--)
    {
        *outL = *inL;
        *outR = *inR;
       
        float outputSample = oscillatorPtr.get()->nextSample();
        
        *outL = outputSample; //   output
        *outR = outputSample;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/**  */
void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    oscillatorPtr.get()->setSampleRate(device->getCurrentSampleRate());
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/**  */
void Audio::audioDeviceStopped()
{

}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/** Controls the amplitude and freq of squareOscillator, sending a 100ms 440Hz impulse when called */
void Audio::beep()
{
    DBG("beep");
    
    oscillatorPtr.get()->setAmplitude(1.0);
    oscillatorPtr.get()->setFrequency(440.0);

    
    uint32 time = Time::getMillisecondCounter();
    Time::waitForMillisecondCounter(time + 100);
    
    oscillatorPtr.get()->setAmplitude(0.0);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/**  */
void Audio::setOscillatorWaveform(int waveformItemIndex)
{
    if (waveformItemIndex == 0)
    {
        oscillatorPtr = new SinOscillator();
    }
    else if (waveformItemIndex == 1)
    {
        oscillatorPtr = new SquareOscillator();
    }
    else if (waveformItemIndex == 2)
    {
        oscillatorPtr = new SawtoothOscillator();
    }
    else if (waveformItemIndex == 3)
    {
        oscillatorPtr = new TriangleOscillator();
    }
    else
        DBG("ERROR - setOscillatorWaveform");
}
