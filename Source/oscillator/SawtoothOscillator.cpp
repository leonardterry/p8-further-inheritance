//
//  SawtoothOscillator.cpp
//  JuceBasicAudio
//
//  Created by Leonard Terry on 10/11/2016.
//
//

#include "SawtoothOscillator.h"

float SawtoothOscillator::renderWaveShape (const float currentPhase)
{
    float out;

    if (currentPhase == 0){
        out = 1.0;
    }
    else if (currentPhase > 0 && currentPhase <= 2 * M_PI){
        out = (currentPhase / M_PI) -1;
    }
    
    return out;
}