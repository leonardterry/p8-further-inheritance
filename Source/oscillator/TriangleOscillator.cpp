//
//  TriangleOscillator.cpp
//  JuceBasicAudio
//
//  Created by Leonard Terry on 10/11/2016.
//
//

#include "TriangleOscillator.h"

float TriangleOscillator::renderWaveShape (const float currentPhase)
{
    float out;
    
    if (currentPhase == 0){
        out = 1.0;
    }
    else if (currentPhase > 0 && currentPhase <= M_PI){
        out = (currentPhase / M_PI) -1;
    }
    else if (currentPhase > M_PI && currentPhase <= 2 * M_PI)
    {
        out = (-currentPhase / M_PI) - 1;
    }
    
    return out;
}