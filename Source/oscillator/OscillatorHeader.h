//
//  OscillatorHeader.h
//  JuceBasicAudio
//
//  Created by Leonard Terry on 10/11/2016.
//
//

/** This header file stores all the header files associated with Oscillator and is called by Audio.h */

#ifndef OscillatorHeader_h
#define OscillatorHeader_h

#include "../oscillator/Oscillator.h"
#include "../oscillator/SinOscillator.h"
#include "../oscillator/SquareOscillator.h"
#include "../oscillator/SawtoothOscillator.h"
#include "../oscillator/TriangleOscillator.h"

#endif /* OscillatorHeader_h */
