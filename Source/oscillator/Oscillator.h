//
//  Oscillator.h
//  JuceBasicAudio
//
//  Created by Leonard Terry on 10/11/2016.
//
//

#ifndef Oscillator_h
#define Oscillator_h

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"


/**
 Abstract class for an oscillator
 */

class Oscillator 
{
public:
    //==============================================================================
    /** Oscillator constructor */
    Oscillator();
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Oscillator destructor */
    virtual ~Oscillator();
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** sets the frequency of the oscillator */
    void setFrequency (float freq);
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** sets frequency using a midi note number */
    void setNote (int noteNum);
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** sets the amplitude of the oscillator */
    void setAmplitude (float amp);
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** resets the oscillator */
    void reset();
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** sets the sample rate */
    void setSampleRate (float sr);
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** Returns the next sample */
    float nextSample();
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    /** pure virtual function that provides the execution of the waveshape */
    virtual float renderWaveShape (const float currentPhase) = 0;
    
protected:
    float phase;
    
private:
    float frequency;
    float amplitude;
    float sampleRate;
    float phaseInc;
};

#endif /* Oscillator_h */
