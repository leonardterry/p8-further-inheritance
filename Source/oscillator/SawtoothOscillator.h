//
//  SawtoothOscillator.hpp
//  JuceBasicAudio
//
//  Created by Leonard Terry on 10/11/2016.
//
//

#ifndef SawtoothOscillator_h
#define SawtoothOscillator_h

#include "Oscillator.h"

/**
 Class for a sawtooth wave oscillator
 */

class SawtoothOscillator : public Oscillator
{
public:
    
    /**
     function that provides the execution of the waveshape
     */
    float renderWaveShape (const float currentPhase) override;
};

#endif /* SawtoothOscillator_h */
