//
//  TriangleOscillator.hpp
//  JuceBasicAudio
//
//  Created by Leonard Terry on 10/11/2016.
//
//

#ifndef TriangleOscillator_h
#define TriangleOscillator_h

#include "Oscillator.h"

/**
 Class for a triangle wave oscillator
 */

class TriangleOscillator : public Oscillator
{
public:
    
    /**
     function that provides the execution of the waveshape
     */
    float renderWaveShape (const float currentPhase) override;
};

#endif /* TriangleOscillator_h */
